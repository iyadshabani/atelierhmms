package com.gestionsalle;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;



import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	/** NFC **/
	NfcAdapter nfcAdapter; //declaration variable qui contiendra les donn�es d'un tag NFC lu
	private static final String SOAP_ACTION = "http://ConferanceWebServicepackage/GetListInscrit";
	private static final String METHOD_NAME = "GetListInscrit";
	private static final String NAMESPACE = "http://ConferanceWebServicepackage/";
	private static final String URL = "http://10.136.100.246:8080/ConferanceInscription/ConferanceWebservice";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initNfc(); //initiation de la variable NFC
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onResume(){
		super.onResume();
		//isInscrit("hajar");// pour le test 3ml hajar
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
		
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
	        Tag leTag = this.getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
	        String idTag = ByteArrayToHexString(leTag.getId());            
	        Log.d("Id", idTag);    
	        boolean existe=isInscrit(idTag);
	        if(existe==true){
  				Toast.makeText(MainActivity.this, " Inscrit (y)",Toast.LENGTH_SHORT).show();
  			 }else{
  				Toast.makeText(MainActivity.this, "Error :  Non  Inscrit !!!!!",Toast.LENGTH_SHORT).show();
  			 }
  			
	        
	           
	    }
	}
	
	@Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
        setIntent(intent);
        Log.d("New Intent", "New intent");
    }
	
	void initNfc() {
		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
	}
	
	String ByteArrayToHexString(byte [] inarray) //conversion en chaine de caract�re hexad�cimale
    {
    int i, j, in;
    String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
    //String [] hex = {"0-9a-fA-F"};
    String out= "";

    for(j = 0 ; j < inarray.length ; ++j) 
        {
        in = (int) inarray[j] & 0xff;
        i = (in >> 4) & 0x0f;
        out += hex[i];
        i = in & 0x0f;
        out += hex[i];
        }
    return out;
    }
	
	boolean isInscrit(String tag){
		boolean existe=false;
			
		SoapObject request=new SoapObject(NAMESPACE,METHOD_NAME);
      //  Toast.makeText(MainActivity.this, "__"+((EditText)findViewById(R.id.txttag)).getText().toString()+"__",Toast.LENGTH_SHORT).show();
       
        
  
		  SoapSerializationEnvelope soapEnvlope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
		  soapEnvlope.dotNet=true;
		  soapEnvlope.setOutputSoapObject(request);
		  HttpTransportSE aht=new HttpTransportSE(URL);
		
		try{
			
			aht.call(SOAP_ACTION, soapEnvlope);
			SoapObject result = (SoapObject)soapEnvlope.bodyIn;
			String Result = result.getProperty(0).toString();// hna fonction geslistinscrit katjbedli ga3 les tags lli deja inscrits 
			Log.e("Result !!!!",Result);
		//	Toast.makeText(MainActivity.this, "koko "+Result,Toast.LENGTH_SHORT).show();
			
			String[] list_etudiant=Result.split("__");
			 for (int i = 0; i <list_etudiant.length; i++) {
	              
               String tag_temp=list_etudiant[i];
				 if(tag_temp.equalsIgnoreCase(tag)){
		
				 existe=true;
				 break;
				 }
             
          }
			 
			 Log.e("Existance !!!!",existe+"");
			 
			
			
		}catch(Exception ex){
			Toast.makeText(MainActivity.this, "Erruer de connexion avec le webservice!", Toast.LENGTH_SHORT).show();
			ex.printStackTrace();
		}
		
		return existe;
	}
}
