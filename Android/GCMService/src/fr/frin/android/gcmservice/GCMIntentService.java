package fr.frin.android.gcmservice;
 
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
 
public class GCMIntentService extends GCMBaseIntentService
{
	private static final String LOG_TAG = "GCMIntentService";
	private static final String PROJECT_ID = "xxxxxxxxxxxx";
 
	/**
	 * Constructeur
	 */
	public GCMIntentService()
	{
		//Définition du Project ID
		super(PROJECT_ID);
	}
 
	/**
	 * Réception d'une erreur lors d'un enregistrement ou d'une déconnexion au service
	 *
	 * @param context Contexte de l'application
	 * @param Identifiant de l'erreur retourné par le service GCM
	 */
	@Override
	protected void onError(Context context, String errorId)
	{
		Log.d(LOG_TAG, "Erreur reçue : " + errorId);
	}
 
	/**
	 * Réception d'un message
	 *
	 * @param context Contexte de l'application
	 * @param intent Intent Contenant le message
	 */
	@Override
	protected void onMessage(Context context, Intent intent)
	{
		Log.d(LOG_TAG, "Message reçu");
		generateNotification(context, intent.getStringExtra("message"));
	}
 
	/**
	 * Enregistrement au service GCM
	 *
	 * @param context Contexte de l'application
	 * @param registrationId Identifiant d'enregistrement
	 */
	@Override
	protected void onRegistered(Context context, String registrationId)
	{
		Log.d(LOG_TAG, "Enregistrement au service GCM avec identifiant " + registrationId);
	}
 
	/**
	 * Déconnexion du service GCM
	 *
	 * @param context Contexte de l'application
	 * @param registrationId Identifiant d'enregistrement
	 */
	@Override
	protected void onUnregistered(Context context, String registrationId)
	{
		Log.d(LOG_TAG, "Déconnexion du service GCM de l'identifiant "+ registrationId);
	}
 
	/**
	 * Génération de la notification
	 *
	 * @param context Contexte de l'application
	 * @param message Message reçu
	 */
	private static void generateNotification(Context context, String message)
	{
		//Récupération de la date
		long when = System.currentTimeMillis();
 
		//Définition du gestionnaire de notification
		NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
 
		//Définition de la notification
		Notification notification = new Notification(R.drawable.ic_launcher, message, when);
 
		Intent notificationIntent = new Intent(context,PushActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		 
		PendingIntent intent = PendingIntent.getActivity(context, 0,notificationIntent, 0);
		notification.setLatestEventInfo(context, context.getString(R.string.app_name), message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		 
		//Génération de la notification
		notificationManager.notify(0, notification);
	}
}