package com.push.pushactivity;

import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
 
import com.google.android.gcm.GCMBaseIntentService;
 
public class GCMIntentService extends GCMBaseIntentService
{
  private static final String LOG_TAG = "GCMIntentService";
  private static final String PROJECT_ID = "25682353319";
 
 /**
  * Constructeur
  */
  public GCMIntentService()
  {
    //D�finition du Project ID
    super(PROJECT_ID);
  }
 
 /**
  * R�ception d'une erreur lors d'un enregistrement ou d'une d�connexion au service
  *
  * @param context Contexte de l'application
  * @param Identifiant de l'erreur retourn� par le service GCM
  */
  @Override
  protected void onError(Context context, String errorId)
  {
    Log.d(LOG_TAG, "Erreur re�ue : " + errorId);
  }
 
 /**
  * R�ception d'un message
  *
  * @param context Contexte de l'application
  * @param intent Intent Contenant le message
  */
  @Override
  protected void onMessage(Context context, Intent intent)
  {
    Log.d(LOG_TAG, "Message re�u");
    generateNotification(context, intent.getStringExtra("message"));
  }
 
 /**
  * Enregistrement au service GCM
  *
  * @param context Contexte de l'application
  * @param registrationId Identifiant d'enregistrement
  */
  @Override
  protected void onRegistered(Context context, String registrationId)
  {
    Log.d(LOG_TAG, "Enregistrement au service GCM avec identifiant " + registrationId);
  }
 
 /**
  * D�connexion du service GCM
  *
  * @param context Contexte de l'application
  * @param registrationId Identifiant d'enregistrement
  */
  @Override
  protected void onUnregistered(Context context, String registrationId)
  {
    Log.d(LOG_TAG, "D�connexion du service GCM de l'identifiant "+ registrationId);
  }
 
 /**
  * G�n�ration de la notification
  *
  * @param context Contexte de l'application
  * @param message Message re�u
  */
  private static void generateNotification(Context context, String message)
  {
    //R�cup�ration de la date
    long when = System.currentTimeMillis();
 
    //D�finition du gestionnaire de notification
    NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
 
    //D�finition de la notification
    Notification notification = new Notification(R.drawable.ic_launcher, message, when);
 
    Intent notificationIntent = new Intent(context,PushActivity.class);
    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
 
    PendingIntent intent = PendingIntent.getActivity(context, 0,notificationIntent, 0);
    notification.setLatestEventInfo(context, context.getString(R.string.app_name), message, intent);
    notification.flags |= Notification.FLAG_AUTO_CANCEL;
 
    //G�n�ration de la notification
    notificationManager.notify(0, notification);
  }
}
