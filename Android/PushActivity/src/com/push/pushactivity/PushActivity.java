package com.push.pushactivity;

import com.google.android.gcm.GCMRegistrar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
 
public class PushActivity extends Activity
{
  private static final String LOG_TAG = "PushActivity";
  private static final String PROJECT_ID = "xxxxxxx";
 
 /**
  * M�thode appel�e � la cr�ation de l'activit�e
  * 
  * @param savedInstanceState
  */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    Log.d(LOG_TAG, "D�marrage PushActivity");
 
    super.onCreate(savedInstanceState);
    setContentView(R.layout.push);
 
    //R�cup�ration du textView
    TextView registrationNumber = (TextView)findViewById(R.id.registrationNumber);
 
    //V�rification des pr�-requis pour l'utilisation de GCM
    GCMRegistrar.checkDevice(this);		
    GCMRegistrar.checkManifest(this);
 
    //R�cup�ration du code d'enregistrement
    final String registrationId = GCMRegistrar.getRegistrationId(this);
 
    //Si aucun code d'enregistrement
    if (registrationId.equals(""))
    {
      // Enregistrement du service
      GCMRegistrar.register(this, PROJECT_ID);
    }
 
    //On indique le code dans le textView
    registrationNumber.setText(registrationId);
    Log.d(LOG_TAG, "Reg ID : " + registrationId);
  }
}
