package com.example.clientandroid;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

 

public class MainActivity extends Activity

{

	/*private static final String SOAP_ACTION = "http://ConferanceWebServicepackage/GetInfoByTag";
	private static final String METHOD_NAME = "GetInfoByTag";
	private static final String NAMESPACE = "http://ConferanceWebServicepackage/";
	private static final String URL = "http://197.230.39.17:8080/ConferanceInscription/ConferanceWebservice";*/
	private static final String SOAP_ACTION = "http://www.webserviceX.NET/ConversionRate";
	private static final String METHOD_NAME = "ConversionRate";
	private static final String NAMESPACE = "http://www.webserviceX.NET/";
	private static final String URL = "http://www.webservicex.net/CurrencyConvertor.asmx";
    Button btnFar,btnCel,btnClear;
    EditText txtFar,txtCel;
    

    @Override

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnFar = (Button)findViewById(R.id.btnFar);
        txtFar = (EditText)findViewById(R.id.txttag);
        ((ImageView)findViewById(R.id.imgerreur)).setVisibility(View.GONE);
  	  ((ImageView)findViewById(R.id.imgvalide)).setVisibility(View.GONE);
        
        btnFar.setOnClickListener(new View.OnClickListener()

        {
                  @Override
                  public void onClick(View v)
                  {
                	  
                	 
                  Toast.makeText(MainActivity.this, "Hajar",Toast.LENGTH_SHORT).show();
                  SoapObject request=new SoapObject(NAMESPACE,METHOD_NAME);
                  Toast.makeText(MainActivity.this, "__"+((EditText)findViewById(R.id.txttag)).getText().toString()+"__",Toast.LENGTH_SHORT).show();
                  String tag=((EditText)findViewById(R.id.txttag)).getText().toString();
                 request.addProperty("IdEtudiant",tag);
                  
            
          		  SoapSerializationEnvelope soapEnvlope=new SoapSerializationEnvelope(SoapEnvelope.VER11);
          		  soapEnvlope.dotNet=true;
          		  soapEnvlope.setOutputSoapObject(request);
          		  HttpTransportSE aht=new HttpTransportSE(URL);
          		
          		try{
          			
          			aht.call(SOAP_ACTION, soapEnvlope);
          			SoapObject result = (SoapObject)soapEnvlope.bodyIn;
          			String Result = result.getProperty(0).toString();
          			
          			Toast.makeText(MainActivity.this, "koko "+Result,Toast.LENGTH_SHORT).show();
          			boolean existe=false;
					String etudiant_diyana="";
          			String[] list_etudiant=Result.split("_ _ _");
          			 for (int i = 0; i <list_etudiant.length; i++) {
          	              
                         String etudiant=list_etudiant[i];
						 if(etudiant.split("__")[0].equalsIgnoreCase(tag)){
						 etudiant_diyana=etudiant;
						 existe=true;
						 break;
						 }
                       
                    }
          			
          			/*SoapPrimitive resultString =(SoapPrimitive)soapEnvlope.getResponse();
          			*/
          			
          			
          			if(existe==true){
          				((EditText)findViewById(R.id.txttag)).setText(etudiant_diyana.split("__")[0]);
          				((EditText)findViewById(R.id.txtnom)).setText(etudiant_diyana.split("__")[1]);
          				((EditText)findViewById(R.id.txtprenom)).setText(etudiant_diyana.split("__")[2]);
          				 ((ImageView)findViewById(R.id.imgerreur)).setVisibility(View.GONE);
          			  	  ((ImageView)findViewById(R.id.imgvalide)).setVisibility(View.VISIBLE);
          			}else{
          				 ((ImageView)findViewById(R.id.imgerreur)).setVisibility(View.VISIBLE);
          			  	  ((ImageView)findViewById(R.id.imgvalide)).setVisibility(View.GONE);
          				
          			}
          			//txtCel.setText(" "+Result);
          			
          		}catch(Exception ex){
          			Toast.makeText(MainActivity.this, "Erruer de connexion avec le webservice!", Toast.LENGTH_SHORT).show();
          			ex.printStackTrace();
          		}
          		
               
                  }

            });

       

    }

}
