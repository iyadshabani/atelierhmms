-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 14 Mai 2014 à 17:29
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `conferance_inscription`
--
CREATE DATABASE IF NOT EXISTS `conferance_inscription` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `conferance_inscription`;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE IF NOT EXISTS `etudiant` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `idetudiant` text,
  `nom` text,
  `prenom` text,
  `email` text,
  `rowid_salle` int(11) NOT NULL,
  PRIMARY KEY (`rowid`),
  KEY `FK_etudiant_rowid_salle` (`rowid_salle`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `etudiant`
--
-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE IF NOT EXISTS `salle` (
  `rowid_salle` int(11) NOT NULL AUTO_INCREMENT,
  `numero_salle` text,
  `description` text,
  PRIMARY KEY (`rowid_salle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `salle`
--

INSERT INTO `salle` (`rowid_salle`, `numero_salle`, `description`) VALUES
(-1, '000', '00');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `FK_etudiant_rowid_salle` FOREIGN KEY (`rowid_salle`) REFERENCES `salle` (`rowid_salle`);
  

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
