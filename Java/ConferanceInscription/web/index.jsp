

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Inscription</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Fancy Sliding Form with jQuery" />
        <meta name="keywords" content="jquery, form, sliding, usability, css3, validation, javascript"/>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript" src="sliding.form.js"></script>
    </head>
    <style>
        span.reference{
            position:fixed;
            left:5px;
            top:5px;
            font-size:10px;
            text-shadow:1px 1px 1px #fff;
        }
        span.reference a{
            color:#555;
            text-decoration:none;
			text-transform:uppercase;
        }
        span.reference a:hover{
            color:#000;
            
        }
        h1{
            color:#ccc;
            font-size:36px;
            text-shadow:1px 1px 1px #fff;
            padding:20px;
        }
    </style>
    <body>
      
        <div id="content">
            <h1>l'inscription dans la conférance</h1>
            <div id="wrapper">
                <div id="steps">
                    <form id="formElem"  action="traitement_inscription" method="post">
                        
                        <fieldset class="step">
                            <legend>Saisies tes informations</legend>
                            <p>
                                <label for="name">Id Etudiant</label>
                               <input type="text" name="idetudiant" value="" required/>
                            </p>
                            <p>
                                <label for="country">Nom</label>
                                <input type="text" name="nom" value="" required/>
                            </p>
                            <p>
                                <label for="phone">Prenom</label>
                                <input type="text" name="prenom" value="" required/>
                            </p>
                            <p>
                                <label for="website">Email</label>
                               <input type="email" name="email" value="" required />
                            </p>
							<p class="submit">
                                <button id="registerButton" type="submit">Valider</button>
								
                            </p>
                        </fieldset>
                        
                        
						
                    </form>
                </div>
               
            </div>
        </div>
    </body>
</html>