/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ConferanceWebServicepackage;

import java.sql.ResultSet;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import metiere.Etudiant;
import metiere.MonhibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author YNS
 */
@WebService(serviceName = "ConferanceWebservice")
public class ConferanceWebservice {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }


    @WebMethod(operationName = "GetListInscrit")
    public String GetListInscrit() {
          SessionFactory ss= MonhibernateUtil.getSessionFactory();
        Session sess= ss.openSession();
        sess.beginTransaction();
        String list_tags="";
       List lst= sess.createQuery("From Etudiant").list();
            for (int i = 0; i < lst.size(); i++) {
              
                 Etudiant bt=(Etudiant)lst.get(i);
                 String tag=bt.getIdetudiant();
                 list_tags+= bt.getIdetudiant()+"__";       
                
            }
        sess.close();
        return list_tags;
 
    }
    
    
}
