/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package traitement;

import metiere.MonhibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import metiere.Etudiant;
import metiere.Salle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author YNS
 */
@WebServlet(name = "traitement_inscription", urlPatterns = {"/traitement_inscription"})
public class traitement_inscription extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet traitement_inscription</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet traitement_inscription at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nom=request.getParameter("nom");
        String prenom=request.getParameter("prenom");
        String email=request.getParameter("email");
        String Idetudiant=request.getParameter("idetudiant");
                
        boolean ok=existeIdEtudiant(Idetudiant);
        if (ok==true) {
            HttpSession sessionaa=request.getSession();
            sessionaa.setAttribute("message", "sorry! had Id deja inscrit");
            response.sendRedirect("message.jsp");
            
        }else{
            
            SessionFactory ss= MonhibernateUtil.getSessionFactory();// kanft7o session diyal hibernate
            Session sess= ss.openSession();
            sess.beginTransaction();
            Etudiant et=new Etudiant();
            et.setNom(nom);
            et.setPrenom(prenom);
            et.setEmail(email);
            et.setIdetudiant(Idetudiant);
            
            Salle salle_default=new Salle();
            salle_default.setRowidSalle(-1);
            et.setSalle(salle_default);
            sess.save(et);
            sess.getTransaction().commit();
            sess.close();
            HttpSession sessionaa=request.getSession();
            sessionaa.setAttribute("message", "<font color='green'> bien insrit , attends le message de la direction</font>");
            response.sendRedirect("message.jsp");
            
        }
    }
    
     public boolean existeIdEtudiant(String IdEtudiant){
        SessionFactory ss= MonhibernateUtil.getSessionFactory();
        Session sess= ss.openSession();
        sess.beginTransaction();
        List lst= sess.createQuery("From Etudiant").list();
        for(int i=0;i<lst.size();i++){
            Etudiant bt=(Etudiant)lst.get(i);
            if (bt.getIdetudiant().equalsIgnoreCase(IdEtudiant)) {
                
                return true;
            }
        }
        sess.close();
        return false;
    
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
